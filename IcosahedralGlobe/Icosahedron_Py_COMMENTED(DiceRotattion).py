
#####Initialization#####
#we start by importing the libraries that we might need for the code

#importing rhinoscript library in order to code in rhino framework
import rhinoscriptsyntax as rs

#importing Rhino.Geometry library in order to deal with geometries in rhino
import Rhino.Geometry as rg

#importing math library in order to do more sophisticated calculations
import math 



#####Functions#####
#in order to facilitate our workflow we would create small tools to help us
#each function that we define here is a tool that we later use in the main body of the code


#this function draws the Icosahedron as a series of mesh faces
# The inputs defined in below (BP, R)
# BP is the 'base plane' input which you can see in the grasshopper script 
# R is the radius of your object also defined outside in your grasshopper script
def Icosahedron(BP,R):
    #https://en.wikipedia.org/wiki/Regular_icosahedron
    #The main process is that we are going to create our icosahedron based 
    #on 3 golden rectangles that they are prependecular to each other
    #https://en.wikipedia.org/wiki/Regular_icosahedron#/media/File:Icosahedron-golden-rectangles.svg

    #first step is to calculate phi or the golden ratio
    phi = 0.5 * (1 + math.sqrt(5))
    
    #second we need to redefine the radius (R)
    R = R / (math.sqrt(1 + phi * phi))

    #third thing that we need to do is to extract the axis of the base plane
    #in order to construct planes for the 3 golden rectangles
    e1 = BP.XAxis
    e2 = BP.YAxis
    e3 = BP.ZAxis

    #4th we need to construct planes by choosing the extracted axes 2 at a time as a basis
    P12 = rg.Plane(BP.Origin, e1, e2)
    P23 = rg.Plane(BP.Origin, e2, e3)
    P31 = rg.Plane(BP.Origin, e3, e1)

    #5th step is to find the corners of this imaginary golden rectangle
    # the width of the golden rectangle is 2 times R (2*R) and the length of it 2 times golden ration times R (2*phi*R)
    #since the origin point of the plane is the center of the rectangle, the range between -R and +R is 2*R which is the width
    # and the range between -R*phi and +R*phi is 2*phi*R which is the length of the rectangle
    #(advanced point: the main reason that we redfined R in the 2nd step is that it would be multiplied by phi here again
    #and the radious of the whole geometry would be exactly R)

    #method "PointAt" creates a point on a plane
    #in each of these steps we go through all the planes and add new points on their corners
    #plane 1: XY Plane
    V12_NE = P12.PointAt(+R * phi, +R)
    V12_NW = P12.PointAt(-R * phi, +R)
    V12_SW = P12.PointAt(-R * phi, -R)
    V12_SE = P12.PointAt(+R * phi, -R)
    
    #plane 2: YZ Plane
    V23_NE = P23.PointAt(+R * phi, +R)
    V23_NW = P23.PointAt(-R * phi, +R)
    V23_SW = P23.PointAt(-R * phi, -R)
    V23_SE = P23.PointAt(+R * phi, -R)
    #(advanced point: here we add one additional point because we want to cut the geometry from here and project it in
    # to a rectangle)
    V23_AA = P23.PointAt(-R * phi, 0)
    
    #plane 3: ZX Plane
    V31_NE = P31.PointAt(+R * phi, +R)
    V31_NW = P31.PointAt(-R * phi, +R)
    V31_SW = P31.PointAt(-R * phi, -R)
    V31_SE = P31.PointAt(+R * phi, -R)

    #6th Step: when drawing any mesh in rhino we need to initialize an empty mesh (we call Icosahedron) before hand
    #later we will add verticies to it
    #we initialize our Icosahedron:
    Icosahedron = rg.Mesh()
    
    #7th step: here we put all the corners of our golden rectangles that we have created in step 5 in a list
    #(advanced point: we add our additional point for cutting (V23_AA) at the end of the list, pay attention to the naming)
    vertices = [V12_NE,V12_NW,V12_SW,V12_SE,V23_NE,V23_NW,V23_SW,V23_SE,V31_NE,V31_NW,V31_SW,V31_SE,V23_AA]
    
    #8th step: since we are lazy (like every other coder) we create a for loop incrementaly add each point in 
    #our list to our mesh (Icosahedron) as a vertex instead of adding them individually
    for point in vertices:
        Icosahedron.Vertices.Add(point)

    #9th step: after adding all the vertecies to the mesh, we know that they are added in an order and now each vertex has an index
    #hence, when we want to give instruction for the construction of each face, we will give them as sequence of indecies
    #"AddFace" method is used to give this instruction in order to construct a meshface
    #(important) rhino understands how to draw a face based on the order of points it is given, as we learned in class, everything is drawn
    #in a clockwise fashion. e.g if you look at point 0, 7, 4, and use the right hand rule, the normal will face outwards

    #these 5 faces are 5 upper most faces
    Icosahedron.Faces.AddFace(0, 7, 4)
    Icosahedron.Faces.AddFace(0, 9, 7)
    Icosahedron.Faces.AddFace(9, 0, 3)
    Icosahedron.Faces.AddFace(8, 3, 0)
    Icosahedron.Faces.AddFace(0, 4, 8)

    #these 10 faces are the middle faces
    Icosahedron.Faces.AddFace(1, 4, 7)
    #(advanced point:here we have commented out one of the faces and added two smaller triangles since we know that
    #our geometry is going to be cut from here, notice vertex number 12 which is the added vertex for cut line)
    #Icosahedron.Faces.AddFace(3, 5, 6)
    Icosahedron.Faces.AddFace(3, 5, 12)
    Icosahedron.Faces.AddFace(3, 12, 6)
    Icosahedron.Faces.AddFace(4, 11, 8)
    Icosahedron.Faces.AddFace(5, 8, 11)
    Icosahedron.Faces.AddFace(6, 10, 9)
    Icosahedron.Faces.AddFace(7, 9, 10)
    Icosahedron.Faces.AddFace(1, 11, 4)
    Icosahedron.Faces.AddFace(3, 8, 5)
    Icosahedron.Faces.AddFace(1, 7, 10)
    Icosahedron.Faces.AddFace(3, 6, 9)

    #these are 5 faces at the lower part of the geometry
    Icosahedron.Faces.AddFace(2, 10, 6)
    Icosahedron.Faces.AddFace(10, 2, 1)
    Icosahedron.Faces.AddFace(11, 1, 2)
    Icosahedron.Faces.AddFace(2, 5, 11)
    #(advanced point: again replacing one triangles with 2 half triangles because of the cut line)
    #Icosahedron.Faces.AddFace(2, 6, 5)
    Icosahedron.Faces.AddFace(2, 6, 12)
    Icosahedron.Faces.AddFace(2, 12, 5)
    
    #10th step: the output of this function is the mesh called icosahedron which we need to return it at the end.
    return Icosahedron

#this function converts the cartesian coordinate system of our geometry to LLA system (longitude and latitude)
# The inputs defined in below (x,y,z,R)
# x,y,z are the coordinate of the point that we need to convert  
# R is the radius of your object also defined outside in your grasshopper script
# (advanced point: in this function we have assumed that the center of the globe is on the 0,0,0 aka origin point)
def ECEFtoLLA(x,y,z,R):
    # ECEF2LLA - convert earth-centered earth-fixed (ECEF)
    #            cartesian coordinates to latitude, longitude,
    #            and altitude

    # Notes: (1) This function assumes the WGS84 model.
    #        (2) Latitude is customary geodetic (not geocentric).
    #        (3) Inputs may be scalars, vectors, or matrices of the same
    #            size and shape. Outputs will have that same size and shape.
    #        (4) Tested but no warranty; use at your own risk.
    #        (5) Michael Kleder, April 2006

    # WGS84 ellipsoid constants:
    #R = 6378137 this is the actual  radius of the earth that we may use later
    #e = 8.1819190842622e-2 this is the eliptical deviation of the earth
    #we have set e to 0 since we dont consider the actual eliptical shape of the earth
    e = 0
    
    # calculations:
    # if you are curious we did not come up with the calculation of the conversion, we just found them on the internet
    b   = math.sqrt(R**2*(1-e**2))
    ep  = math.sqrt((R**2-b**2)/b**2)
    p   = math.sqrt(x**2+y**2)
    th  = math.atan2(R*z,b*p)
    lon = math.atan2(y,x)
    lat = math.atan2((z+ep**2*b*math.sin(th)**3),(p-e**2*R*math.cos(th)**3))
    N   = R/math.sqrt(1-e**2*math.sin(lat)**2)
    alt = p/math.cos(lat)-N

    # we convert the radians to degree
    lat = 180 * lat / math.pi #convert to degrees
    lon = 180 * lon / math.pi #convert to degrees
    
    #we return the longitude and latitude of the point as a "tuple" (google it)
    return (lon, lat)


# This function will create and rotate (we will explain in detail) the icosahedron with a coordination of a north pole and south pole 
# The inputs defined in below (BP, R)
# BP is the 'base plane' input which you can see in the grasshopper script 
# R is the radius of your object also defined outside in your grasshopper script
def IcosahedralGlobe(BP, R):
    #    #((important)): here we are using one of the function that we have wrote earliear
    #    #this is important to understand that every small tool that you create can be used in order to make more sophisticated tools
    #    #we use our tool to draw the basic icosahedron
    #    IcosahedralMesh = Icosahedron(BP, R)
    #    #here we calculate the golden ration
    #    phi = 0.5 * (1 + math.sqrt(5))
    #    #here we calculate the angle (theta) that we need to rotate our geometry in order for each pole to have a vertex
    #    theta = math.asin(1 / math.sqrt(1 + phi * phi))
    #
    #    #here we rotate them
    #    IcosahedralMesh.Rotate(-theta, BP.ZAxis, BP.Origin)
    #    IcosahedralMesh.Rotate(-0.5 * math.pi, BP.YAxis, BP.Origin)
    #    IcosahedralMesh.Rotate(-0.5 * math.pi, BP.ZAxis, BP.Origin)
    #
    #    #then we return the rotated object
    #    return IcosahedralMesh
    
    IcosahedralMesh = Icosahedron(BP, R)
    phi = 0.5 * (1 + math.sqrt(5))
    theta = math.asin(1 / math.sqrt(1 + phi * phi))
    
    v0 = IcosahedralMesh.Vertices[0]
    v4 = IcosahedralMesh.Vertices[4]
    v8 = IcosahedralMesh.Vertices[8]
    DiceZAxis = rg.Vector3d(v0 + v4 + v8)
    DiceZAxis.Unitize()
    DiceYAxis = v4 - v8
    DiceYAxis.Unitize()
    DiceXAxis = rg.Vector3d.CrossProduct(DiceYAxis, DiceZAxis)
    DiceXAxis.Unitize()
    DicePlane = rg.Plane(BP.Origin, DiceXAxis, DiceYAxis)
    xForm = rg.Transform.PlaneToPlane(DicePlane, BP)
    DiceMesh = IcosahedralMesh.DuplicateMesh()
    DiceMesh.Transform(xForm)
    
    
    DicePlaneBasis = DicePlane
    return DiceMesh



#####MainBody#####
# here we write the main body of our code and we use all the functions (small tools) that we have created above

# 1st we call the Globe function which creates (within itself) and rotates the basic Icosahedron
IcosahedralGlobeMesh = IcosahedralGlobe(BP, R)

# 2nd we return this geometry in output A
A = IcosahedralGlobeMesh

# 3rd we extract a list of verticies from the geometry
vertices = IcosahedralGlobeMesh.Vertices.ToPoint3dArray()

#4th we initialize an empty list to append our LLA cordinates later
LonLatCoordinates = []

#5th in this for loop, we iterate through our list of vertieces and convert their coorditanes into LLA
for i in range(len(vertices)):
    #puuting the cuurent vertex in a variable
    P = vertices[i]
    #converting its coordinate by calling our own function
    LonLat = ECEFtoLLA(P.X, P.Y, P.Z, R)
    #adding it to the list
    LonLatCoordinates.append(LonLat)
    #repeating the whole process again

#6th we return this new coordinates in Output B in our GH module
B = LonLatCoordinates

#7th we extract a list of meshfaces from the mesh
faces = IcosahedralGlobeMesh.Faces

#8th we return this list of meshfaces in output C
C = faces

#9th we initialize another empty list called file, which we later will fill it with the necessary strings
file = []

#10th we define a counter and set it to 0
counter = 0

#11th since the text file that we want to produce needs a header, we will append that header as the first string in our list (file)
file.append("id|wkt")

#12th here we will iterate through our faces and convert their construction instruction into 
#WKT which is readable format for QGIS (where all the data is)
#This process is necessary since we need to export our unique way of tesselating the globe into the QGIS8
#This is an example of the format:
#0|Polygon((0.0 89.999998280823164,-36.000001828023947 26.565050645823117,36.000001828023947 26.565050645823117,0.0 89.999998280823164))
#id|type((1st_vertex_x 1st_vertex_y,2nd_vertex_x 2nd_vertex_y,3rd_vertex_x 3rd_vertex_y,1st_vertex_x 1st_vertex_y))
#notice that the 1st vertex is repeated at the end to close the polygon
#this is a for loop which iterates through each face
for face in faces:
    #before anything, notice that in this kind of loop, in each iteration one of the meshfaces of the geometry is put in the variable "face"
    
    #12.1 outstring is the main container for our string in each iteration
    #here we initialize it by adding the id (counter)* and then adding the string "|Polygon((" which is part of the format
    #*attention, counter is an integer (google it) which we need to convert to a string (google it)
    outstring = str(counter) + "|Polygon(("

    #12.2 (advanced point: in ourder to project this geometry exactly to a rectangle we need to cut it
    # for the verticies that fall on the cut line we have a special case
    # when they are called out as part of the meshface which is on the right of the cut line, their longitude shoud be positive
    # in the oposite case their longitude should be negative
    # in order to figure out that on which side of the cut line is the mesh face, we need to find their center points
    # which we extract in the next line
    centeroid = faces.GetFaceCenter(counter)

    #12.3 here we will iterate through the series of numbers equivalent to the numbers of the vertieces per face
    for k in range(3):
        #12.4 here we extract the index of the vertex of the face that we are working on
        VertexIndex = face[k]
        
        #12.5 then we extract the LLA cordinates of them from the list that we have calculated earlier and put it in the variable "CurrentVertex"
        CurrentVertex = B[VertexIndex]
        #12.6 if the longitude of a vertex is 180 or -180 it means that it has fallen on the cutline
        # by creating an if condition, we are checking if the absolute of the longitude is equal to 180
        # by indicating that we want the first value of Current Vertex, which is the longitude, all we have to do 
        # is use the first item index which is 0, if we want the latitude we give the second item index which is 1
        #NOTE remember we start counding with 0 in coding:) dont let this confuse you
        if abs(CurrentVertex[0]) == 180:
            # 12.7 dividing any number by its absolute value is an easy indicator of the sign, the result being 1 or -1
            # by looking at the centroid of the face and checking if the y-coordinate is positive or negative, we are checking whether that vertex
            # should be on the positive side of the cut line or negative side 
            sign = (centeroid.Y+ + 0.001)/(abs(centeroid.Y) + 0.001)
            #12.8 here we convert the longitude e to the correct sign based on on what side of the cut line the face that contains this coordinate it on
            CurrentVertex = (abs(CurrentVertex[0]) * sign, CurrentVertex[1])
        # 12.9 here we initialize another variable called vertex
        # we start by adding the string conversion of CurrentVertex defined above using str()--- Google it
        vertex = str(CurrentVertex)
        # 12.10 we use a method called translate which converts the value in the second attribute (in this case "(") to "None", which effectively deletes it
        vertex = vertex.translate(None, "(")
        vertex = vertex.translate(None, ")")
        vertex = vertex.translate(None, ",")
        # 12.11 here we add the vertex coordinates and a "," to our variable "outstring"  
        outstring = outstring + vertex + ","
        # 12.12 since we need to add the first vertex to the end of the instruction string (this is simply a formatting thing that we need to follow)
        # we will save the first vertex in a seperate variable.
        # the first vertex is when k = 0
        if k==0:
            firstvertex = vertex
    #12.13 here we add the first vertex to the end of the string with an addition "))" (again formatting stuff, no brains involved)
    outstring = outstring + firstvertex + "))"
    #12.14  here again we append the outstring to the empty list we defeined way above called "file"
    file.append(outstring)
    #12.15 IMPORTANT this you will see alot, this is an manual way to update the counter after the loop is finished,
    #therefore the next time this iterates the counter wont be 0, but it will be 1, than 2, etc etc
    counter = counter + 1
#step13_ OUTPUT it duh!
D = file
