# -*- coding: utf-8 -*-
"""
Created on Fri May 18 22:16:58 2018

@author: Park
"""

import time


#limiting total area
def clamp(n, minn, maxn):
    if n < minn:
        return minn
    elif n > maxn:
        return maxn
    else:
        return n
    
#settin dummy dataset
population = 60
area_of_urban = 100
density = (population/area_of_urban)*100
happiness = 100
area_of_forest = 500
area_of_agri = 50
food_satisfaction = (area_of_agri*5)/(population*3)*100
Total_area = area_of_urban + area_of_forest + area_of_agri
Free_space = 2000 - Total_area
clamp(Total_area,0,2000)



# welcoming user and explaning
name = raw_input('please set your name:')
print 'Hello '+name,', welcome to test simulation'
time.sleep(2)
explanation = raw_input('Do you want to listen explanation of the game?:(y/n)')
if explanation == 'y':
    print 'The goal of the program is to make people happy'
    time.sleep(2.5)
    print ' by making enough space and increase their happiness'
    time.sleep(2.5)
    print ' You are going to set area of urban and agriculture land.'
    time.sleep(2.5)
    print 'but there is always consequences of you descison.'
    time.sleep(2.5)
    print ' Okay the game will start in 3 seconds, good luck'
    time.sleep(1)
    print '1..'
    time.sleep(1)
    print '2...'
    time.sleep(1)
    print '3....'
    time.sleep(1)
elif explanation =='n':
    print 'I will just explain to you!'
    time.sleep(2.5)   
    print 'The goal of the program is to make people happy'
    time.sleep(2.5)
    print 'by making enough space and increase their happiness'
    time.sleep(2.5)
    print ' You are going to set area of urban and agriculture land.'
    time.sleep(2.5)
    print 'but there is always consequences of you descison.'
    time.sleep(2.5)
    print ' Okay the game will start in 3 seconds, good luck'
    time.sleep(1)
    print '1..'
    time.sleep(1)
    print '2...'
    time.sleep(1)
    print '3....'
    time.sleep(1)
    
print '####                Status               #####'
print ("Current urban area is %d" %area_of_urban)
print ("Current agriculture area is %d" %area_of_agri)
print ("Current forest area is %d" %area_of_forest)
print ("Current population is %d" %population)
print ('Current food satisfaction is %d' %food_satisfaction)
print ("Current happiness is %d" %happiness)
print ("you have %d of free space" %Free_space)
print 'you have 10 turns.'
   

turns = 0

while turns< 11 and happiness > 0 :
    
    #setting turns
    turns += 1
    turn_left = 10 - turns
    
    
    #sliders of Landuses
    new_urban_area = int(raw_input('how much area do you want to set more urban?:from -40 to 100:'))
    area_of_urban += new_urban_area
        
    new_agri_area = int(raw_input('how much area do you want to set more agriculture?:from -40 to 50:'))
    area_of_agri += new_agri_area
           

    Threat_for_forest = new_agri_area *0.5
    #area_of_forest *= 0.8
    area_of_forest -= Threat_for_forest
    
    population *= 1.3
    density = (population/area_of_urban)*100
    
    
    #food satisfaction
    if food_satisfaction >= 1:
        happiness += 10
        population *= 1.15
    else:
        happiness -=10
        population *=0.7
        
        
    
    # Think about winning condition
    
    
    # Failure condition with land limit
    if Total_area >= 2000:
        print ' you cannot put anymore area'
        print ' game ends'
        break
       
    #Density and happiness problem
    if density >60:
        happiness -= 15
    if density > 100:
        happiness -= 40
    
    if happiness <= 0:
        population -= 100
        print '       '
        print '######             Comment           #######'
        print 'People start to commit suicide because of stress'
        print 'Hmm... you should have done something'
        print 'maybe some policy could have helped you'
        
        
    #Policy applied, not sure how to limits usage of policy
    if area_of_forest < 200:
        print ''
        print '#########                Alert           ########'
        print ' There is not enough forest'
        print ' '
        print 'we would like to introduce a policy for you'
        print 'we have green belt policy'
        print 'Ex - it increases 500 of forest area'
        new_forest = raw_input('Do you want to activate the policy?[y/n]')
        if new_forest =='y':
            area_of_forest = area_of_forest+500
    
    
    if happiness >= 100:
       area_of_forest += 10
       print 'People are happy and they start to care forest'
       time.sleep(2)
       print 'area of forest will increase by 10'          
      
           
            
           

    
     # Failure condition with Density limit
    if density >85:
        print ' '    
        print 'I would say this is not good.'  
        print ' '
        print 'Please check your density, you should increase urban area'
        print ' '
    
    print '                '
    print '############                Status          ###############'
    print ('you have just set %d more urban area .' %new_urban_area)
    print ('you have just set %d more agriculture area .' %new_agri_area)
    print ("Current urban area is %d" %area_of_urban)
    print ("Current agriculture area is %d" %area_of_agri)
    print ("Current forest area is %d" %area_of_forest)
    print ("Current population is %d" %population)
    print ('Current food satisfaction is %d' %food_satisfaction)
    print ("Current density is %d percent" %density)
    print ("Current happiness is %d" %happiness)
    print ("you have %d of free space" %Free_space)
    print 'you have %d turns.' %turn_left
   
     
else:
    print '##########      Result      ########### '
    print ('you just finished the first phase')
    print ("your final urban area is %d" %area_of_urban)
    print ("your final agriculture area is %d" %area_of_agri)
    print ("your final forest area is %d" %area_of_forest)
    print ("population is %d" %population)
    print ("Your is %d percent" %density)
    print ('your food satisfaction is %d' %food_satisfaction)
    print ("and happiness is %d" %happiness)
    
    

    
    

   
        


