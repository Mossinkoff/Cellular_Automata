"""
BZZZ ZZZy  ZZZZZZZ  ,ZZZZZZZ  ZZZB ZZZ    ZZZZZz    ZZZ,ZZZj  ZZZZ ZZZ       ZZZ BZZZ  DZZZ,  ZZZZ  ZZZZ
 ZZ   Z5    Zj w 5   wZ   j,   zZ  ZW    ZZ   yZE   DZw  Z9    Z Zw ZW       Zy ZZ     9Z     Z,Zj Z Z
 ZZZZZZz    ZZZZ     jZZZz      8ZZD     ZZ    ZZ   zZj  ZZ    Z 8Z Zy       ZZZZ      ZZ     Z EZ Z Z
 ZZ   ZD    Zy  DZ   WZ   ZB     ZZ      ZZj  ZZD   8ZD  ZZ    Z  Z8Z5       ZD 8Zy    EZ     Z  ZZj Z
8ZZZ ZZZD  ZZZZZZZ  WZZZZZZ9    ZZZZ      jZZZZ      8ZZZZ    ZZZ wZZz       ZZZy9ZZZ  zZZZw  ZZZ ZZ ZZZ

20180620 @Heeyoun Kim
2018 Why Factory_Future Model_Team Energy

1. This py module will use to calculate 318 triangles which are not chosen.
2. The Iteration should be happend in the houdini
3. Basically, this module will use same classes calc_engine, and distribution_Controller
"""


from distribution_Controller import distribution_Controller as dc
from parameter_Controller import parameter_Controller as pc
from calc_Engine import calc_energygame as ce
print('NOTICE : import modules')

#################################################################
################ D U M M Y     D I C T I O N R Y ################
#################################################################

# turn_test variable
triangle_a = {'land': 1, 'mountain': 0, 'urban': 0, 'population': 39843100, 'population_default': 39843100,
                  'temperature': 20.79, 'temp_delta': 0.9,
                  'energy_demands': 10, 'pollution': 10, 'energy_satisfaction': 1, 'transition_value': 0.1,
                  'oil_reserves': 63689300, 'oil_production': 5547860,
                  'coal_reserves': 1176220000, 'coal_production': 2653140,
                  'gas_reserves': 52601100, 'gas_production': 5600540,
                  'solar_potential': 2, 'rain_potential': 0, 'wind_potential': 0, 'water_potential': 0,
                  'total_potential': 2

                  }

###################################
### F U N C T I O N   S T A R T ###
###################################

def non_select_tri():

    #################
    ### C A L L S ###
    #################

    turn_in = 1
    engine = ce()
    b = pc()
    dist = dc()

    ###################################################
    #### !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ####
    #### This part will not be used in the houdini ####
    #### !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ####
    ####                                           ####
    ####                                           ####
    ####                                           ####
    ####                                           ####
    oil_production = input("how much oil will you produce this turn?: ")
    coal_production = input("how much coal will you produce this turn?: ")
    gas_production = input("how much gas will you produce this turn?: ")

    b.oil_getter(oil_production)
    b.coal_getter(coal_production)
    b.gas_getter(gas_production)

    print("You product oil: ", b.inputvalue_oil)
    print("You product coal: ", b.inputvalue_coal)
    print("You product Gas: ", b.inputvalue_gas)
    ####                                           ####
    ####                                           ####
    ####                                           ####
    ####                                           ####
    #### !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ####
    ###################################################




    ##########################
    ### B A S E  V A L U E ###
    ##########################

    population_a = engine.population_calc(triangle_a['population'], turn_in, triangle_a['pollution'])

    temperature_a = triangle_a['temperature']

    energy_capita_a = engine.en_calc(temperature_a)

    energy_demand_a = engine.ed_calc(energy_capita_a, population_a)

    renewable_demand_a = engine.rd_calc(energy_demand_a, triangle_a['transition_value'])

    fossil_demand_a = engine.fd_calc(energy_demand_a, renewable_demand_a)


    #################################
    ### F O S S I L E   F U E L S ###
    #################################

    oil_prod_a = engine.non_tri_oil_prod(triangle_a['oil_reserves'], fossil_demand_a)
    oil_reserve_a = engine.oil_reserve(triangle_a['oil_reserves'], oil_prod_a)

    coal_prod_a = engine.non_tri_coal_prod(triangle_a['coal_reserves'], fossil_demand_a)
    coal_reserve_a = engine.coal_reserve(triangle_a['coal_reserves'], coal_prod_a)

    gas_prod_a = engine.non_tri_gas_prod(triangle_a['gas_reserves'], fossil_demand_a)
    gas_reserve_a = engine.gas_reserve(triangle_a['gas_reserves'], gas_prod_a)

    fossil_prod_a = engine.fossil_prod(oil_prod_a, coal_prod_a, gas_prod_a)



    #########################
    ### R E N E W A B L E ###
    #########################

    renew_solar_a = triangle_a['solar_potential']
    renew_rain_a = triangle_a['rain_potential']
    renew_wind_a = triangle_a['wind_potential']
    renew_water_a = triangle_a['water_potential']
    renew_total_a = triangle_a['total_potential']

    solar_prod_a = engine.renew_solar_prod(renew_solar_a, renew_total_a, renewable_demand_a)
    rain_prod_a = engine.renew_rain_prod(renew_rain_a, renew_total_a, renewable_demand_a)
    wind_prod_a = engine.renew_wind_prod(renew_wind_a, renew_total_a, renewable_demand_a)
    water_prod_a = engine.renew_water_prod(renew_water_a, renew_total_a, renewable_demand_a)
    renew_total_prod_a = engine.renew_total_prod(solar_prod_a, rain_prod_a, wind_prod_a, water_prod_a)

    #####################
    ### O V E R A L L ###
    #####################

    overall_prod_a = engine.overall_production(fossil_prod_a, renew_total_prod_a)

    #################
    ### D E L T A ###
    #################

    delta_a = engine.delta_calc(energy_demand_a, overall_prod_a)

    ################################
    ### E N E R G Y  S U P P L Y ###
    ################################

    energy_supply_a = engine.non_tri_energy_supply(delta_a, energy_demand_a)

    #########################
    ### P O L L U T I O N ###
    #########################
    pollution_a = engine.pol_calc(fossil_prod_a)

    #############################
    ### T E M P E R A T U R E ###
    #############################

    temp_change_a = engine.temp_change(pollution_a)
    temp_delta_a = engine.temp_delta(triangle_a['temp_delta'], temp_change_a)
    temp_a = engine.temperature(triangle_a['temperature'], temp_change_a)

    ###################################################
    ### P S Y C H O L O G I C A L   D I S T A N C E ###
    ###################################################

    psy_distance_a = engine.psy_distance(temp_delta_a)


    #################################
    ###  C I T Y   V A C A N C Y  ###
    #################################

    city_empty_a = 1

    ###############################
    ### T R A N S   V A L U E #####
    ###############################

    transition_a = engine.trans_value(triangle_a['transition_value'], psy_distance_a, city_empty_a)
