___________                                    ________                    .__    .______  ___ 
\_   _____/ ____   ___________  ____ ___.__.  /  _____/___________  ______ |  |__ |__\   \/  / 
 |    __)_ /    \_/ __ \_  __ \/ ___<   |  | /   \  __\_  __ \__  \ \____ \|  |  \|  |\     /  
 |        \   |  \  ___/|  | \/ /_/  >___  | \    \_\  \  | \// __ \|  |_> >   Y  \  |/     \  
/_______  /___|  /\___  >__|  \___  // ____|  \______  /__|  (____  /   __/|___|  /__/___/\  \ 
        \/     \/     \/     /_____/ \/              \/           \/|__|        \/         \_/ 
                                                                                                   
Author: Alex Kypriotakis-Weijers
Planet Makers Spring 2018
Energy Group


This hudini file contains several digital assets to make your globe a bit more pleasant to see and look more like a video game!
All assets are placed according to planet data. Color of the land surface and vegetation for instance are based on temperature
data. Same for mountains, Cities, coasts and so on. You will see the "refugee" asset which was theme-defined for our group but you
can use it however you want. There is a lot of room for even more assets to be added but be carefull with
cloud SOPs as they will take up a lot of memmory to run if you are planning to run animations. 



Enjoy and I hope it proves useful to ya! =)